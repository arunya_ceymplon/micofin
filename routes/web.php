<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/create', 'HomeController@create');

Route::get('/add', 'HomeController@add');

Route::get('/subtract', 'HomeController@subtract');
Route::get('/delete', 'HomeController@delete');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => config('nova.middleware')], function () {
    Route::get('/nova-assets/scripts/{script}', 'ScriptController@show');
    Route::get('/nova-assets/styles/{style}', 'StyleController@show');
});
