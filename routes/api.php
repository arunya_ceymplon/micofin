<?php

use Illuminate\Http\Request;
use App\GramaNiladhari;
use App\Account;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Location\Coordinate;
use Location\Polygon;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/location',function(Request $request){

    $selected_gnd= $request->input('selected_gnd');
    $lat=$request->input('lat');
    $lng=$request->input('lng');

        $datas=\App\GramaNiladhari::find($selected_gnd);

        $d=json_decode($datas);

        $geofence = new Polygon();
        foreach ($d->geometry_data->geometries[0]->coordinates[0] as  $coordinate)
        {
            $geofence->addPoint(new Coordinate($coordinate[1], $coordinate[0]));

        }

        $insidePoint = new Coordinate($lat,$lng);
        return response()->json($geofence->contains($insidePoint));

});






Route::get('/getLoanspaidamount/{account}', function ($account) {

    $loan_details = Account::find(1000)->loans()->first();
    $loan_id = $loan_details->id;

    // dd($loan_details->id);
    $events_details= DB::table('stored_events')->get();

    $loans = $events_details->map(function ($data) use ($loan_id) {

        $content = json_decode($data->event_properties, true);

        if ($content["loan_id"] == $loan_id) {
            return [
                $content["amount"],
                \Carbon\Carbon::parse($data->created_at)->format('d/m/Y'),
                $content["officer"],
            ];
        }

    });


    $array=[];
    foreach($loans as $loan)
    {
        if($loan !=null)
        {
            array_push($array,array('amount'=>$loan[0],'date'=>$loan[1],'officer'=>$loan[2]));
        }
    }
    $events=array_slice(array_reverse($array), 0, 3, true);

    return response()->json(array('events'=>$events,'user_details'=>$loan_details));

});

Route::middleware('auth:api')->get('/subtract', 'HomeController@subtract');



