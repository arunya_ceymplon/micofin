<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->increments('id');
            
            $table->double('disbursed_amount');
            $table->double('balance_amount')->nullable();
            $table->date('opened_on')->nullable();
            $table->date('matured_on')->nullable();
            $table->float('interest_rate')->nullable();

            $table->timestamps();
        });
         DB::update("ALTER TABLE loans AUTO_INCREMENT = 1000;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
