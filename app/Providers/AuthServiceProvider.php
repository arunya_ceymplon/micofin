<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Silvanite\Brandenburg\Traits\ValidatesPermissions;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{

    use ValidatesPermissions;
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        \App\User::class => \App\Policies\PermissionPolicy::class,
        \App\Loan::class => \App\Policies\LoanPolicy::class,
        \App\Account::class => \App\Policies\AccountPolicy::class,
        \App\GramaNiladhari::class => \App\Policies\GramaNiladhariPolicy::class,
        \App\Branch::class => \App\Policies\BranchPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        {
            collect([
                'viewPermission',
                'managePermission',
            ])->each(function ($permission) {
                Gate::define($permission, function ($user) use ($permission) {
                    if ($this->nobodyHasAccess($permission)) {
                        return true;
                    }
                    return $user->hasRoleWithPermission($permission);
                });
            });
            $this->registerPolicies();

            Passport::routes();
        }
    }
}
