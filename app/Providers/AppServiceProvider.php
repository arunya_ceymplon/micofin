<?php

namespace App\Providers;

use App\Projectors\AccountBalance;
use App\Projectors\AccountBalancepro;
use App\Projectors\BalanceEvent;
use Illuminate\Support\ServiceProvider;
use Spatie\EventProjector\Projectionist;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Projectionist $projectionist)
    {
       // $projectionist->addProjector(AccountBalancepro::class);
        $projectionist->addProjector(BalanceEvent::class);
    }
}
