<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class AccountLoan extends Pivot
{
    Protected $table='account_loan';
}
