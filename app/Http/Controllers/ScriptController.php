<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Nova;

class ScriptController extends Controller
{
    /**
     * Serve the requested script.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return \Illuminate\Http\Response
     */
    public function show(NovaRequest $request)
    {
        $path = Nova::allScripts()[$request->route('script')] ?? null;
        abort_unless($path, 404);

        return response(
            file_get_contents($path),
            200, ['Content-Type' => 'application/javascript']
        );
    }
}


