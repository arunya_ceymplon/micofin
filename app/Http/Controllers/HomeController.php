<?php

namespace App\Http\Controllers;

use App\Loan;
use App\Account;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }



    public function subtract(Request $request){

        $account_id= $request->input('account_id');
        $amount=$request->input('amount');
        $loan_officer=$request->input('loan_officer');
        $loanAccount = Account::find($account_id)->Loans()->first();
        $loanAccount->subtractMoney($amount,$loan_officer);
        return response()->json(true);



    }

}
