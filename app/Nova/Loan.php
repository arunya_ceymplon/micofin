<?php

namespace App\Nova;

use App\Nova\Actions\AddAccount;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;

use Laravel\Nova\Http\Requests\NovaRequest;
use phpDocumentor\Reflection\Types\Float_;

class Loan extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Loan';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make('id')->sortable(),
           
            Number::make('disbursed_amount'),

            Number::make('balance_amount'),
            Date::make('opened_on')
                ->rules('required', 'date', 'date_format:Y-m-d'),
            Date::make('matured_on')
                ->rules('required', 'date', 'date_format:Y-m-d','after:date_of_commencement'),
            Text::make('interest_rate'),
           // BelongsToMany::make('Accounts'),
            BelongsToMany::make('Account','accounts', Account::class) ->actions(function () {
                return [
                    new Actions\AddAccount(),
                ];
            }),


        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [ new AddAccount,];
    }
}
