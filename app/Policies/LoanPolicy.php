<?php

namespace App\Policies;

use App\User;
use App\Loan;
use Illuminate\Auth\Access\Gate;
use Illuminate\Auth\Access\HandlesAuthorization;

class LoanPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the loan.
     *
     * @param  \App\User  $user
     * @param  \App\Loan  $loan
     * @return mixed
     */
    public function view(User $user, Loan $loan)
    {
        if($user->hasRoleWithPermission('viewPermission')){
            return true;
        }
    }

    /**
     * Determine whether the user can create loans.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if($user->hasRoleWithPermission('managePermission'))
        {
            return true;
        }
    }

    /**
     * Determine whether the user can update the loan.
     *
     * @param  \App\User  $user
     * @param  \App\Loan  $loan
     * @return mixed
     */
    public function update(User $user, Loan $loan)
    {
        if($user->hasRoleWithPermission('managePermission'))
        {
            return true;
        }
    }

    /**
     * Determine whether the user can delete the loan.
     *
     * @param  \App\User  $user
     * @param  \App\Loan  $loan
     * @return mixed
     */
    public function delete(User $user, Loan $loan)
    {
        if($user->hasRoleWithPermission('managePermission'))
        {
            return true;
        }
    }

    /**
     * Determine whether the user can restore the loan.
     *
     * @param  \App\User  $user
     * @param  \App\Loan  $loan
     * @return mixed
     */
    public function restore(User $user, Loan $loan)
    {
        return Gate::any(['viewPermission', 'managePermission'],$user, $loan);

    }

    /**
     * Determine whether the user can permanently delete the loan.
     *
     * @param  \App\User  $user
     * @param  \App\Loan  $loan
     * @return mixed
     */
    public function forceDelete(User $user, Loan $loan)
    {
        if($user->hasRoleWithPermission('managePermission'))
        {
            return true;
        }
    }
}
