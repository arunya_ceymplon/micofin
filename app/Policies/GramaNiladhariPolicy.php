<?php

namespace App\Policies;

use App\User;
use App\GramaNiladhari;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Gate;
class GramaNiladhariPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the grama niladhari.
     *
     * @param  \App\User  $user
     * @param  \App\GramaNiladhari  $gramaNiladhari
     * @return mixed
     */
    public function view(User $user, GramaNiladhari $gramaNiladhari)
    {
        if($user->hasRoleWithPermission('viewPermission'))
        {
            return true;
        }
    }

    /**
     * Determine whether the user can create grama niladharis.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {

        if($user->hasRoleWithPermission('managePermission'))
        {
            return true;
        }
    }

    /**
     * Determine whether the user can update the grama niladhari.
     *
     * @param  \App\User  $user
     * @param  \App\GramaNiladhari  $gramaNiladhari
     * @return mixed
     */
    public function update(User $user, GramaNiladhari $gramaNiladhari)
    {
        if($user->hasRoleWithPermission('managePermission'))
        {
            return true;
        }
    }

    /**
     * Determine whether the user can delete the grama niladhari.
     *
     * @param  \App\User  $user
     * @param  \App\GramaNiladhari  $gramaNiladhari
     * @return mixed
     */
    public function delete(User $user, GramaNiladhari $gramaNiladhari)
    {
        if($user->hasRoleWithPermission('managePermission'))
        {
            return true;
        }
    }

    /**
     * Determine whether the user can restore the grama niladhari.
     *
     * @param  \App\User  $user
     * @param  \App\GramaNiladhari  $gramaNiladhari
     * @return mixed
     */
    public function restore(User $user, GramaNiladhari $gramaNiladhari)
    {
        return Gate::any(['viewPermission', 'managePermission'],$user, $gramaNiladhari);
    }

    /**
     * Determine whether the user can permanently delete the grama niladhari.
     *
     * @param  \App\User  $user
     * @param  \App\GramaNiladhari  $gramaNiladhari
     * @return mixed
     */
    public function forceDelete(User $user, GramaNiladhari $gramaNiladhari)
    {
        if($user->hasRoleWithPermission('managePermission'))
        {
            return true;
        }
    }
}
