<?php

namespace App;

use App\Events\AccountCreated;
use App\Events\AccountDeleted;
use App\Events\MoneyAdded;
use App\Events\MoneySubtracted;
use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    protected $fillable = ['disbursed_amount'];
    protected $casts = ['opened_on' => 'date',
                       'matured_on' => 'date'];


    public function accounts()
    {
        return $this->belongsToMany(Account::class,'account_loan' ,'loan_id','account_id');
    }


    public static function createWithAttributes(array $attributes){
        /*
         * Let's generate a uuid
         */
//        $attributes['id']=(string) Uuid::uuid4();

        /*
         * The account will be created inside this event using the generated uuid
         */
        event(new AccountCreated($attributes));
        /*
         * The uuid will be used the retrieve the created account
         */
         //return static::find($attributes['id']);
    }



    public function addMoney(int $amount){
        event(new MoneyAdded($this->id,$amount));
    }
    public function subtractMoney(int $amount,string $loanofficer){
        event(new MoneySubtracted($this->id,$amount,$loanofficer));
    }

    public function remove(){
        event(new AccountDeleted($this->id));
    }



}
