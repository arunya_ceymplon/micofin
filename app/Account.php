<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Account extends Model
{

    protected $guarded = [];
    public function loans()
    {
        return $this->belongsToMany(Loan::class);
    }

    public function gramaniladhari()
    {
        return $this->belongsTo(GramaNiladhari::class);
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }





}
