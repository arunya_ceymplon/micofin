<?php

namespace App\Events;



use Spatie\EventProjector\ShouldBeStored;

class MoneySubtracted implements ShouldBeStored
{


        /** @var string */
        public $loan_id;

        /** @var int */
        public $amount;
        public $id;

        /**
         * Create a new event instance.
         *
         * @return void
         */
        public function __construct(int $accountid, int $amount,string $loanofficer )
    {
        $this->loan_id=$accountid;
        $this->amount = $amount;
        $this->id=$loanofficer;

    }




}
