<?php

namespace App\Events;

use Spatie\EventProjector\ShouldBeStored;

class AccountDeleted implements ShouldBeStored

{

    public $accountid;

    public function __construct(string $accountid)
    {
        $this->accountid = $accountid;
    }


}
