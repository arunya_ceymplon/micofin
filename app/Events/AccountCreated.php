<?php

namespace App\Events;


use Spatie\EventProjector\ShouldBeStored;

class AccountCreated implements ShouldBeStored
{

    public $accountAttributes;
    /**
     * Create a new event instance.
     *@var array
     * @return void
     */
    public function __construct(array $accountAttributes)
    {

          $this->accountAttributes = $accountAttributes;
    }


}
