<?php

namespace App\Events;


use Spatie\EventProjector\ShouldBeStored;

class MoneyAdded implements ShouldBeStored
{
    /** @var string */
    public $accountid;

    /** @var int */
    public $amount;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(string $accountid, int $amount)
    {
        $this->accountid=$accountid;
        $this->amount = $amount;
    }

}