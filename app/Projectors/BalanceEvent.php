<?php

namespace App\Projectors;

use App\Events\AccountCreated;
use App\Events\AccountDeleted;
use App\Events\MoneyAdded;
use App\Events\MoneySubtracted;
use App\Loan;
use Spatie\EventProjector\Projectors\Projector;
use Spatie\EventProjector\Projectors\ProjectsEvents;

class BalanceEvent implements Projector
{
    use ProjectsEvents;

    /*
     * Here you can specify which event should trigger which method.
     */
    protected $handlesEvents = [
        // EventHappened::class => 'onEventHappened',
        AccountCreated::class => 'onAccountCreated',
        MoneyAdded::class => 'onMoneyAdded',
        MoneySubtracted::class => 'onMoneySubtracted',
        AccountDeleted::class => 'onAccountDeleted',
    ];

    /*
    public function onEventHappened(EventHappended $event)
    {

    }
    */

    public function onAccountCreated(AccountCreated $event)
    {

         Loan::create($event->accountAttributes);
    }


    public function onMoneyAdded(MoneyAdded $event)
    {
       // dd($event);



        $account = Loan::find($event->accountid);

        $account->disbursed_amount += $event->amount;

        $account->save();
    }


    public function onMoneySubtracted(MoneySubtracted $event)
    {
        $account = Loan::find($event->accountid);

        $account->balance_amount -= $event->amount;

        $account->save();
    }

    public function onAccountDeleted(AccountDeleted $event)
    {
        Loan::find($event->accountid)->delete();
    }
}