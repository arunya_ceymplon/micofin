<?php

namespace App;

use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Model;

class GramaNiladhari extends Model
{

    use SpatialTrait;
    protected $spatialFields = [
        'geometry_data'
        ];
    public function accounts()
{
    return $this->hasMany(Account::class);
}

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
